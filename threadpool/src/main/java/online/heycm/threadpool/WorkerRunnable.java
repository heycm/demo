package online.heycm.threadpool;

import java.util.UUID;
import org.slf4j.MDC;

/**
 * 还可以个性化 Runnable
 *
 * @author heycm
 * @since 2023/11/10 23:15
 */
public abstract class WorkerRunnable implements Runnable {

    private final int workerId;

    public WorkerRunnable(int workerId) {
        this.workerId = workerId;
        MDC.put("TraceID", UUID.randomUUID().toString().replaceAll("-", ""));
    }

    public int getWorkerId() {
        return workerId;
    }
}
