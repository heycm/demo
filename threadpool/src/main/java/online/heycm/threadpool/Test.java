package online.heycm.threadpool;

import java.util.concurrent.ThreadPoolExecutor;
import online.heycm.platform.threadpool.WorkerPoolExecutor;

/**
 * @author heycm
 * @since 2023/11/10 23:08
 */
public class Test {

    private static final WorkerPoolExecutor WORKER_POOL = new WorkerPoolExecutor(1, 4, 100, 3,
            (r, threadPoolExecutor) -> {
                WorkerRunnable wr = (WorkerRunnable) r;
                System.out.printf("当前线程[%d]....啊哦, 被拒绝入队了...%n", wr.getWorkerId());
            },
            (r, throwable) -> {
                WorkerRunnable wr = (WorkerRunnable) r;
                if (throwable != null) {
                    System.out.printf("当前线程[%d]....完蛋, 发生异常了...%n", wr.getWorkerId());
                    return;
                }
                System.out.printf("当前线程[%d]....诶嘿, 执行完成 good good...%n", wr.getWorkerId());
            });

    public static void main(String[] args) {

        System.out.println("===================> 主线程执行...");

        for (int i = 0; i < 200; i++) {

            WORKER_POOL.execute(new WorkerRunnable(i) {
                @Override
                public void run() {
                    System.out.printf("当前线程[%d]....执行业务了......%n", this.getWorkerId());
                    if (this.getWorkerId() == 100) {
                        throw new RuntimeException("发生了异常..");
                    }
                }
            });

        }

        System.out.println("===================> 主线程完成ok.");

    }

}
