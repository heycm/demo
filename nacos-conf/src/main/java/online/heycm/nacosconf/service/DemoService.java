package online.heycm.nacosconf.service;

import online.heycm.nacosconf.config.NacosConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author heycm
 * @since 2023/11/8 21:06
 */
@RestController
@RequestMapping("/demo")
public class DemoService {

    @Autowired
    private NacosConf nacosConf;

    @GetMapping
    public String nacosConf() {
        System.out.println(nacosConf);
        return "ok";
    }

}
