package online.heycm.nacosconf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "online.heycm")
public class NacosConfApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosConfApplication.class, args);
    }

}
