package online.heycm.nacosconf.config;

import lombok.Data;
import online.heycm.platform.nacos.BaseNacosConf;
import org.springframework.stereotype.Component;

/**
 * @author heycm
 * @since 2023/11/8 21:31
 */
@Component
@Data
public class NacosConf extends BaseNacosConf {

    private boolean debugger;

    private int key;

}
