package online.heycm.redis.config;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.annotation.PostConstruct;
import online.heycm.platform.redis.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * @author heycm
 * @since 2023/11/12 11:30
 */
@Configuration
public class RedisConfig {

    @Autowired
    private RedisCache redisCache;

    @PostConstruct
    private void run() {

        int uid = 300000001;
        String token = UUID.randomUUID().toString().replaceAll("-", "");

        redisCache.setNamespace("user:login", uid+"", token, 1000 * 60 * 60);

        HashMap<String, Object> session = new HashMap<>();
        session.put("userId", uid);
        session.put("userName", "张三");
        session.put("loginTime", new Date());

        redisCache.hSetNamespace("user:session", token, session, 1000 * 60 * 60);

        redisCache.hSetNamespace("user:session", "5d7e6012f40b4626b43887ffa1759bfd", "userId", 300000001);
        redisCache.hSetNamespace("user:session", "5d7e6012f40b4626b43887ffa1759bfd", "loginTime", new Date());

        Map<String, Object> map = redisCache.hGetNamespace("user:session", token);
        System.out.println("map = " + map);

        List<Object> values = redisCache.hValues("user:session:" + token);
        System.out.println("values = " + values);
    }
}
