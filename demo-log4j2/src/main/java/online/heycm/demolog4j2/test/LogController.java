package online.heycm.demolog4j2.test;

import lombok.extern.slf4j.Slf4j;
import online.heycm.platform.log.Logger;
import online.heycm.platform.log.LoggerManager;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author hey
 * @version 1.0
 * @date 2023/12/26 15:24
 */
@RestController
public class LogController {

    public static final Logger log = LoggerManager.getLogger(LogController.class);

    @Autowired
    Test test;

    @GetMapping("/test")
    public String test() {
        MDC.put("TraceId", UUID.randomUUID().toString().replaceAll("-", ""));
        MDC.put("StepId", "1");
        log.info("测试日志，链路ID和顺序ID: [{}|{}]", "TraceId", "StepId");
        test.log();
        MDC.remove("TraceId");
        MDC.remove("StepId");
        return "ok";
    }

}
