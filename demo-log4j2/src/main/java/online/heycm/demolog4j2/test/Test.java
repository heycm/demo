package online.heycm.demolog4j2.test;

import lombok.extern.slf4j.Slf4j;
import online.heycm.platform.log.Logger;
import online.heycm.platform.log.LoggerManager;
import org.springframework.stereotype.Component;

/**
 * @author hey
 * @version 1.0
 * @date 2023/12/26 16:10
 */
@Component
public class Test {

    public static final Logger log = LoggerManager.getLogger(Test.class);

    public void log() {
        log.debug("test debug");
        log.info("test info");
        log.warn("test warn");
        log.error("test error");

        // log4j2.isThreadContextMapInheritable=true
        // 子线程会继承MDC
        new Thread(() -> {
            log.info("测试子线程继承MDC...");
            log.debug("Thread debug");
            log.info("Thread info");
            log.warn("Thread warn");
            log.error("Thread error");
        }).start();


        log.info("haha info");
        log.info("haha info");
        log.info("haha info");
    }

}
