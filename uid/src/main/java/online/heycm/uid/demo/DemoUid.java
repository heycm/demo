package online.heycm.uid.demo;

import javax.annotation.PostConstruct;
import online.heycm.platform.uid.UidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * @author heycm
 * @since 2023/11/26 18:29
 */
@Configuration
public class DemoUid {

    @Autowired
    private UidService uidService;

    @PostConstruct
    public void test() {

        for (int i = 0; i < 100; i++) {
            long nextId = uidService.nextId();

            System.out.println("nextId = " + nextId);
        }

        //AtomicLong a = new AtomicLong(0);
        //long l = System.currentTimeMillis();
        //while (true) {
        //    long nextId = uidService.nextId();
        //    a.incrementAndGet();
        //    if (a.longValue() % 1000000 == 0) {
        //        System.out.println("耗时: " + (System.currentTimeMillis()-l) + "ms, " + a.longValue());
        //        Thread.sleep(500);
        //        l = System.currentTimeMillis();
        //    }
        //    if (a.longValue() >= 10000000) {
        //        break;
        //    }
        //}

    }

}
